import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {HomeComponent} from "./screen/home/home.component";
import {AboutComponent} from "./screen/about/about.component";
import {ContactComponent} from "./screen/contact/contact.component";
import {LoginComponent} from "./screen/login/login.component";
import {RegisterComponent} from "./screen/register/register.component";
import { PagenotfoundComponent } from './screen/pagenotfound/pagenotfound.component';
const routes: Routes = [
  {
    path : "",
    component : HomeComponent,
    pathMatch : "full"
  },
  {
    path : "home",
    component : HomeComponent
  },
  {
    path : "about",
    component : AboutComponent
  },
  {
    path : "contact",
    component : ContactComponent
  },
  {
    path : "login",
    component : LoginComponent
  },
  {
    path : "register",
    component : RegisterComponent,
  },
  {
    path : "**",
    component :PagenotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
